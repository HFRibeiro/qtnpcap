#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "npcapSDK/Include/pcap.h"

#include <sched.h>
#include <pthread.h>
#include <unistd.h>

#include <QDebug>



#define LINE_LEN 16
#define MAX_PACKS 30000

QString IpCheck = "192.168.1.149";
int initialIndex = 9;

QStringList IPS;

bool runThread1 = false,runThread2 = false;

typedef struct tagMY_CONTEXT {
    int count;
} MY_CONTEXT, *PMY_CONTEXT;

pthread_t tid1,tid2;
MY_CONTEXT context;

typedef struct ip_track
{
    QString ip;
    QString country;
    QString region;
    QString city;
    QString ISP;
    int lag;
    int oldTime;
    int bufferNum[MAX_PACKS];
    int recievedCount;
    int mean;
    int max;
    int min;
    int hops;
    int hops_sum;
    int num_packs;
}ip_track;

struct ip_track tracking[50];
int tracking_count = 0;

QStringList IpTrace;

int packetNumber = 0, packetLoss = 0, packetResend = 0;

//QLineSeries *series = new QLineSeries();


/* 4 bytes IP address */
typedef struct ip_address
{
    u_char byte1;
    u_char byte2;
    u_char byte3;
    u_char byte4;
}ip_address;

/* IPv4 header */
typedef struct ip_header
{
    u_char	ver_ihl;		// Version (4 bits) + Internet header length (4 bits)
    u_char	tos;			// Type of service
    u_short tlen;			// Total length
    u_short identification; // Identification
    u_short flags_fo;		// Flags (3 bits) + Fragment offset (13 bits)
    u_char	ttl;			// Time to live
    u_char	proto;			// Protocol 0->Hop by hop || 6-> TCP || 17 -> UDP
    u_short crc;			// Header checksum
    ip_address	saddr;		// Source address
    ip_address	daddr;		// Destination address
    u_int	op_pad;			// Option + Padding
}ip_header;

/* UDP header*/
typedef struct udp_header
{
    u_short sport;			// Source port
    u_short dport;			// Destination port
    u_short len;			// Datagram length
    u_short crc;			// Checksum
}udp_header;

/* Structure to hold GVSP packet information */
typedef struct _gvsp_packet_info
{
    int    chunk;
    uint8_t  format;
    uint16_t status;
    uint16_t payloadtype;
    uint64_t blockid;
    uint32_t packetid;
    int    enhanced;
    int    flag_resendrangeerror;
    int    flag_previousblockdropped;
    int    flag_packetresend;
} gvsp_packet_info;

MainWindow *mainWindow;

void packet_handler(u_char *param, const struct pcap_pkthdr *header, const u_char *pkt_data);
QJsonObject ObjectFromString(const QString& in);

void* Thread1(void* context)
{
    MY_CONTEXT* displayContext = (MY_CONTEXT*)context;
    while(runThread1)
    {
        pcap_if_t *alldevs;
        pcap_if_t *d;
        int inum;
        int i=0;
        pcap_t *adhandle;
        char errbuf[PCAP_ERRBUF_SIZE];
        u_int netmask;
        char packet_filter[] = "ip and udp";
        struct bpf_program fcode;

        if(pcap_findalldevs(&alldevs, errbuf) == -1)
        {
            qDebug("Error in pcap_findalldevs: %s\n", errbuf);
            exit(1);
        }

        /* Print the list */
        for(d=alldevs; d; d=d->next)
        {
            qDebug("%d. %s", ++i, d->name);
            if (d->description)
                qDebug(" (%s)\n", d->description);
            else
                qDebug(" (No description available)\n");
        }

        //11. \Device\NPF_{1BBBD973-E587-4C80-8BF9-79DBCFBA3546} (Intel(R) Ethernet Connection (7) I219-V)
        inum = displayContext->count;


        for(d=alldevs, i=0; i< inum-1 ;d=d->next, i++);

        /* Open the device */
        /* Open the adapter */
        if ((adhandle= pcap_open_live(d->name,	// name of the device
                                 65536,			// portion of the packet to capture.
                                                // 65536 grants that the whole packet will be captured on all the MACs.
                                 1,				// promiscuous mode (nonzero means promiscuous)
                                 1000,			// read timeout
                                 errbuf			// error buffer
                                 )) == NULL)
        {
            qDebug("\nUnable to open the adapter. %s is not supported by Npcap\n", d->name);
            /* Free the device list */
            pcap_freealldevs(alldevs);
            exit(1);
        }

        /* Check the link layer. We support only Ethernet for simplicity. */
        //if(pcap_datalink(adhandle) != DLT_EN10MB)
        //{
        //    fprintf(stderr,"\nThis program works only on Ethernet networks.\n");
        //    /* Free the device list */
        //    pcap_freealldevs(alldevs);
        //    exit(1);
        //}


        if(d->addresses != NULL)
            /* Retrieve the mask of the first address of the interface */
            netmask=((struct sockaddr_in *)(d->addresses->netmask))->sin_addr.S_un.S_addr;
        else
            /* If the interface is without addresses we suppose to be in a C class network */
            netmask=0xffffff;


        //compile the filter
        if (pcap_compile(adhandle, &fcode, packet_filter, 1, netmask) <0 )
        {
            fprintf(stderr,"\nUnable to compile the packet filter. Check the syntax.\n");
            /* Free the device list */
            pcap_freealldevs(alldevs);
            exit(1);
        }

        //set the filter
        if (pcap_setfilter(adhandle, &fcode)<0)
        {
            fprintf(stderr,"\nError setting the filter.\n");
            /* Free the device list */
            pcap_freealldevs(alldevs);
            exit(1);
        }

        printf("\nlistening on %s...\n", d->description);

        /* At this point, we don't need any more the device list. Free it */
        pcap_freealldevs(alldevs);

        /* start the capture */
        pcap_loop(adhandle, 0, packet_handler, NULL);

        pcap_close(adhandle);
    }
    pthread_exit(0);
}

void* Thread2(void* context)
{
    while(runThread2)
    {
        if(IpTrace.count() >0)
        {
            int index = 0;
            for(int i=0; i<tracking_count;i++) if(tracking[i].ip == IpTrace.at(0)) index = i;

            QProcess *traceroute = new QProcess();
            traceroute->setProcessChannelMode(QProcess::MergedChannels);

            QString comand = "tracert";
            QStringList argsOMX;
            argsOMX.append("/h");
            argsOMX.append("40");
            argsOMX.append(IpTrace.at(0));
            traceroute->start(comand,argsOMX,QIODevice::ReadWrite);

            // Wait for it to start
            while(!traceroute->waitForStarted());

            // Continue reading the data until EOF reached
            QByteArray data;
            int valFinal = 0;
            QString readTotal = "";
            bool timeout = false, gotBefore = false;
            int timeoutCount = 0;
            while(traceroute->waitForReadyRead() && !timeout){
                QString read = traceroute->readAll();
                readTotal += read;
                QStringList lines = readTotal.split("\r\n");
                for(int j=0; j<lines.count();j++)
                {
                    if(lines[j].contains("Request timed out") && gotBefore) timeoutCount++;
                    else if(lines[j].contains("Request timed out") && !gotBefore) gotBefore = true;
                    else gotBefore = false;
                    //qDebug() << "traceroute: " << j << lines[j] << "timeoutCount: " <<  timeoutCount;
                }
                if(timeoutCount > 3) timeout = true;
                timeoutCount = 0;
            }

            QStringList lines = readTotal.split("\r\n");
            for(int j=0; j<lines.count();j++)
            {
                valFinal = 0;
                QString line = lines.at(j);
                if(line.contains("  "))
                {
                    line = line.remove(0,line.indexOf("  "));
                    QStringList pieces = line.split("    ");
                    //for(int k=0;k<pieces.count();k++)  qDebug() << "piece " << k << " :" << pieces.at(k);
                    QString valms = "";
                    if(pieces.count()>1) valms = pieces[1];
                    valms = valms.replace("ms","");
                    valms = valms.replace("<","");
                    valFinal = valms.toInt();
                    tracking[index].hops_sum += valFinal;
                    tracking[index].hops++;
                    qDebug() << IpTrace.at(0) << line << " ADD "<< QString::number(valFinal);
                }
            }
            // Output the data
            qDebug("Done!");
            traceroute->kill();
            IpTrace.pop_front();
        }
        else QThread::sleep(1);

    }
    pthread_exit(0);
}

void MainWindow::initializeModel(){

    QStringList horizontalHeader;
    QStringList verticalHeader;

    //Then in your .cpp file use them like this :

    //Here we are setting your columns

    horizontalHeader.append("IP");
    horizontalHeader.append("Country");
    horizontalHeader.append("Region");
    horizontalHeader.append("City");
    horizontalHeader.append("ISP");
    horizontalHeader.append("LAG");
    horizontalHeader.append("MIN");
    horizontalHeader.append("MAX");
    horizontalHeader.append("HOPS");
    horizontalHeader.append("HOPS_SUM");
    horizontalHeader.append("NUM_PACKS");

    // Here we are creating our model

    modelTable = new QStandardItemModel();

    modelTable->setHorizontalHeaderLabels(horizontalHeader);
    modelTable->setVerticalHeaderLabels(verticalHeader);

    ui->tableView->setModel(modelTable); // This is necessary to display the data on table view
    ui->tableView->verticalHeader()->setVisible(true);
    ui->tableView->verticalHeader()->setDefaultSectionSize(10);
    ui->tableView->setShowGrid(true);
}

void MainWindow::addToModel(int index)
{
    QStandardItem *col1 = new QStandardItem(tracking[index].ip);
    QStandardItem *col2 = new QStandardItem(tracking[index].country);
    QStandardItem *col3 = new QStandardItem(tracking[index].region);
    QStandardItem *col4 = new QStandardItem(tracking[index].city);
    QStandardItem *col5 = new QStandardItem(tracking[index].ISP);
    QStandardItem *col6 = new QStandardItem(QString::number(tracking[index].lag)+" ms");
    QStandardItem *col7 = new QStandardItem(QString::number(tracking[index].min)+" ms");
    QStandardItem *col8 = new QStandardItem(QString::number(tracking[index].max)+" ms");
    QStandardItem *col9 = new QStandardItem(QString::number(tracking[index].hops));
    QStandardItem *col10 = new QStandardItem(QString::number(tracking[index].hops_sum)+" ms");
    QStandardItem *col11 = new QStandardItem(QString::number(tracking[index].num_packs));

    modelTable->appendRow( QList<QStandardItem*>() << col1 << col2 << col3 << col4 << col5 << col6 << col7 << col8 << col9 << col10 << col11);

}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    mainWindow = this;

    ui->lineIP->setInputMask( "000.000.000.000" );
    ui->lineIP->setText(IpCheck);
    addInterfaces();

    initializeModel();
    // Here you can set your data in table view
}

/* Callback function invoked by libpcap for every incoming packet */
void packet_handler(u_char *param, const struct pcap_pkthdr *header, const u_char *pkt_data)
{
    struct tm *ltime;
    char timestr[16];
    ip_header *ih;
    udp_header *uh;
    u_int ip_len;
    u_short sport,dport;
    time_t local_tv_sec;

    /*
     * unused parameter
     */
    (VOID)(param);

    /* convert the timestamp to readable format */
    local_tv_sec = header->ts.tv_sec;
    ltime=localtime(&local_tv_sec);
    strftime( timestr, sizeof timestr, "%H:%M:%S", ltime);

    /* print timestamp and length of the packet */

    /* retireve the position of the ip header */
    ih = (ip_header *) (pkt_data + 14); //length of ethernet header

    /* retireve the position of the udp header */
    ip_len = (ih->ver_ihl & 0xf) * 4;
    uh = (udp_header *) ((u_char*)ih + ip_len);

    /* convert from network byte order to host byte order */
    sport = ntohs( uh->sport );
    dport = ntohs( uh->dport );

    /* print ip addresses and udp ports */

    char srcIpAux[20];
    sprintf(srcIpAux,"%d.%d.%d.%d",ih->saddr.byte1, ih->saddr.byte2, ih->saddr.byte3, ih->saddr.byte4);

    char destIpAux[20];
    sprintf(destIpAux,"%d.%d.%d.%d",ih->daddr.byte1, ih->daddr.byte2, ih->daddr.byte3, ih->daddr.byte4);

    //qDebug() << "Dest Ip: " << destIpAux;

    QString destIp(destIpAux);
    QString srcIp(srcIpAux);

    //QString data = QString::fromLocal8Bit((const char*)pkt_data); // or using local encoding

    QString data = "";

    /* Print the packet */
    for (int i=1; (i < header->caplen + 1 ) ; i++)
    {
        data += (QChar)pkt_data[i-1];
    }


    if(destIp == IpCheck && !srcIp.contains("192.168."))
    {

        byte byte1GEV = pkt_data[42];
        byte byte2GEV = pkt_data[43];

        QString flag1bits = QString("%1").arg(pkt_data[44], 8, 2, QChar('0'));
        QString flag2bits = QString("%1").arg(pkt_data[45], 8, 2, QChar('0'));

        qDebug() << "Type: " << Qt::hex << byte1GEV << Qt::hex << byte2GEV;

        packetNumber++;
        if(flag2bits[7] == '1') packetResend++;
        if(flag2bits[6] == '1') packetLoss++;


        qDebug() << "flags: " << " Error: " << flag2bits[5] << " Dropped: " << flag2bits[6] << " Resend: " << flag2bits[7];

        char timeAux[50];
        sprintf(timeAux,"%s.%.6d len:%d ", timestr, header->ts.tv_usec, header->len);
        QString time(timeAux);
        QString printData = srcIp+" -> "+destIp+" Time: "+time;//+" Data: "+data;

        if(!IPS.contains(srcIp))
        {
           tracking[tracking_count].ip = srcIp;
           tracking[tracking_count].lag = 0;
           tracking[tracking_count].oldTime = header->ts.tv_usec;
           tracking[tracking_count].num_packs++;
           IPS.append(srcIp);
           IpTrace.append(srcIp);
           QMetaObject::invokeMethod(mainWindow, "addToModel", Q_ARG(int, tracking_count));
           QMetaObject::invokeMethod(mainWindow, "postRequest", Q_ARG(QString, srcIp));
           QMetaObject::invokeMethod(mainWindow, "setEditText");
           tracking_count++;
        }
        else
        {
            for(int i=0; i<tracking_count;i++)
            {
                if(tracking[i].ip == srcIp)
                {
                    tracking[i].num_packs++;
                    int tmp = header->ts.tv_usec - tracking[i].oldTime;
                    if(tmp < 0) tmp = tmp + 1000000;
                    //qDebug() << "IP/LAG" << srcIp << tmp;
                    tracking[i].lag = tmp/1000;
                    tracking[i].bufferNum[tracking[i].recievedCount] = tmp;
                    if(tracking[i].recievedCount > 0)
                    {
                        int meanTmp = 0;
                        for(int k=0;k<tracking[i].recievedCount;k++) meanTmp += tracking[i].bufferNum[k];
                        meanTmp = meanTmp/tracking[i].recievedCount;
                        tracking[i].mean = meanTmp/1000;
                        if(tracking[i].max < (tmp/1000))tracking[i].max = tmp/1000;
                        if(tracking[i].min > (tmp/1000) && (tmp/1000))tracking[i].min = tmp/1000;
                        if(tracking[i].max < tracking[i].min) tracking[i].min = tracking[i].max;
                    }
                    else {
                        if((tmp/1000)>0)
                        {
                            tracking[i].mean = tmp/1000;
                            tracking[i].max = tmp/1000;
                            tracking[i].min = tmp/1000;
                        }
                    }
                    if(tracking[i].recievedCount>(MAX_PACKS-1))tracking[i].recievedCount = 0;
                    tracking[i].recievedCount++;
                    tracking[i].oldTime = header->ts.tv_usec;
                    QMetaObject::invokeMethod(mainWindow, "setEditText");
                }
            }
        }

        //qDebug() << printData;
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

//http://ip-api.com/json/149.90.55.17

/*
{"status":"success",
"country":"France",
"countryCode":"FR",
"region":"IDF",
"regionName":"Île-de-France",
"city":"Paris",
"zip":"75000",
"lat":48.8566,
"lon":2.35222,
"timezone":"Europe/Paris",
"isp":"Amazon Technologies Inc",
"org":"AWS EC2 (eu-west-3)",
"as":"AS16509 Amazon.com, Inc.",
"query":"15.188.210.100"}
*/

//https://api.hackertarget.com/mtr/?q=15.188.34.73

//85.56.145.87

void MainWindow::postRequest(const QString &IP)
{
    QNetworkAccessManager * mgr = new QNetworkAccessManager(this);
    connect(mgr,SIGNAL(finished(QNetworkReply*)),this,SLOT(onFinish(QNetworkReply*)));
    connect(mgr,SIGNAL(finished(QNetworkReply*)),mgr,SLOT(deleteLater()));

    mgr->get(QNetworkRequest(QUrl("http://ip-api.com/json/"+IP)));
}

void MainWindow::onFinish(QNetworkReply *rep)
{
    QByteArray bts = rep->readAll();
    QString data = QString(bts);
    QJsonObject Obj = ObjectFromString(data);

    if(!Obj["country"].toString().isEmpty())
    {

        for(int i=0; i<tracking_count;i++)
        {
            if(tracking[i].ip == Obj["query"].toString())
            {
                tracking[i].country = Obj["country"].toString();
                tracking[i].region = Obj["regionName"].toString();
                tracking[i].city = Obj["city"].toString();
                tracking[i].ISP = Obj["isp"].toString();
                setEditText();
            }
        }

    }
        //qDebug() << "IP: " <<  Obj["query"].toString() << Obj["country"].toString() << Obj["regionName"].toString() << Obj["city"].toString() << Obj["isp"].toString();
}

void MainWindow::setEditText()
{
    for(int i=0; i<tracking_count;i++)
    {
       QStandardItem* ip = new QStandardItem(tracking[i].ip);
       modelTable->setItem(i, 0, ip);
       QStandardItem* region = new QStandardItem(tracking[i].region);
       modelTable->setItem(i, 1, region);
       QStandardItem* country = new QStandardItem(tracking[i].country);
       modelTable->setItem(i, 2, country);
       QStandardItem* city = new QStandardItem(tracking[i].city);
       modelTable->setItem(i, 3, city);
       QStandardItem* ISP = new QStandardItem(tracking[i].ISP);
       modelTable->setItem(i, 4, ISP);
       QStandardItem* lag = new QStandardItem(QString::number(tracking[i].lag)+" ms");
       modelTable->setItem(i, 5, lag);
       QStandardItem* min = new QStandardItem(QString::number(tracking[i].min)+" ms");
       modelTable->setItem(i, 6, min);
       QStandardItem* max = new QStandardItem(QString::number(tracking[i].max)+" ms");
       modelTable->setItem(i, 7, max);
       QStandardItem* hops = new QStandardItem(QString::number(tracking[i].hops));
       modelTable->setItem(i, 8, hops);
       QStandardItem* hops_sum = new QStandardItem(QString::number(tracking[i].hops_sum)+" ms");
       modelTable->setItem(i, 9, hops_sum);
       QStandardItem* num_packs = new QStandardItem(QString::number(tracking[i].num_packs));
       modelTable->setItem(i, 10, num_packs);

    }

    ui->n_packets->setText("Num packets: "+QString::number(packetNumber));
    ui->n_resends->setText("Num Resend: "+QString::number(packetLoss));
    float percent = packetLoss*100/packetNumber;
    ui->percentage->setText("Percentage lost: "+QString::number(percent)+"%");
}

QJsonObject ObjectFromString(const QString& in)
{
    QJsonObject obj;
    QJsonDocument doc = QJsonDocument::fromJson(in.toUtf8());

    // check validity of the document
    if(!doc.isNull()){
        if(doc.isObject()) obj = doc.object();
        else qDebug() << "Document is not an object";
    }
    else{
        qDebug() << "Invalid JSON...\n" << in;
    }

    return obj;
}


void MainWindow::on_bt_start_clicked()
{
    if(!runThread1 && !runThread2)
    {
        runThread1 = true;
        runThread2 = true;
        pthread_create(&tid1, NULL, Thread1, &context);
        pthread_create(&tid2, NULL, Thread2, &context);
        ui->label->setStyleSheet("color: rgb(0, 170, 0);");
    }

}

void MainWindow::on_bt_stop_clicked()
{
    if(runThread1 && runThread2)
    {
        runThread1 = false;
        runThread2 = false;
        ui->label->setStyleSheet("color: rgb(170, 0, 0);");
    }
}

void MainWindow::on_cb_interfaces_currentIndexChanged(int index)
{
    qDebug() << "Selected: " << index+1 << ui->cb_interfaces->currentText();
    context.count = index+1;
}

void MainWindow::addInterfaces()
{
    pcap_if_t *alldevs;
    pcap_if_t *d;
    int i=0;
    char errbuf[PCAP_ERRBUF_SIZE];

    if(pcap_findalldevs(&alldevs, errbuf) == -1)
    {
        qDebug("Error in pcap_findalldevs: %s\n", errbuf);
        exit(1);
    }

    /* Print the list */
    for(d=alldevs; d; d=d->next)
    {
        qDebug("%d. %s", ++i, d->name);
        if (d->description){
            QString interfaces = QString(d->description)+" : "+QString(d->name).replace("\\Device\\NPF_{","").replace("}","");
            ui->cb_interfaces->addItem(interfaces);
            qDebug() << "ADD" << i << interfaces;
        }
        else
            qDebug(" (No description available)\n");
    }
    ui->cb_interfaces->setCurrentIndex(initialIndex);
}

void MainWindow::on_bt_clear_clicked()
{
    QStringList tmp = * new QStringList();
    for(int i=0;i<tracking_count;i++)tracking[i] = ip_track();
    tracking_count = 0;
    IPS.clear();
    IpTrace.clear();
    packetNumber = 0, packetLoss = 0, packetResend = 0;
    ui->n_packets->setText("Num packets: 0");
    ui->n_resends->setText("Num resend: 0");
    ui->percentage->setText("Percentage resend: 0%");
    modelTable->clear();
    initializeModel();
}

void MainWindow::on_lineIP_textChanged(const QString &arg1)
{

}

void MainWindow::on_lineIP_editingFinished()
{
    IpCheck = ui->lineIP->text();
    qDebug() << IpCheck;
}
