#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtDebug>
#include <QNetworkAccessManager>
#include <QHttpPart>
#include <QNetworkReply>
#include <QJsonObject>
#include <QJsonDocument>
#include <QLabel>
#include <QStringListModel>
#include <QtMath>
#include <QNetworkRequest>
#include <QProcess>
#include <QThread>
#include <QStandardItemModel>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void onFinish(QNetworkReply *rep);

    void on_bt_start_clicked();

    void on_bt_stop_clicked();

    void on_cb_interfaces_currentIndexChanged(int index);

    void addInterfaces();

    void on_bt_clear_clicked();

    void on_lineIP_textChanged(const QString &arg1);

    void on_lineIP_editingFinished();

    void initializeModel();

    void addToModel(int index);


public:
  Q_SLOT void setEditText();

public slots:
    void postRequest(const QString &IP);

private:
    Ui::MainWindow *ui;
    QStandardItemModel *modelTable;
};
#endif // MAINWINDOW_H
